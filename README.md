# Web USB Labs

## About 

It is a demo lab collections for Web USB API.

## Possible Applications

Although USB is widely used in many applications. Most of the USB devices have been supported by OS level.
For example, USB CDC (as serial port and derivates), USB MSD (as memory stick), USB HID (as mouse and 
keyboard). We still can find some useful applications based upon web technology.

- HID class of Custom Input Devices such as Joystick, barcode reader
- Audio class of music instrument (with MIDI)
- Image class of scanner, camera
- Printer class of custom printer
- DFU and ISP (with USB CDC or JTAG/SWD probes)
- IrDa bridge (legacy device)
- Wireless device class for IoT (with USB dongle of Zigbee/LoRa/Sub-1GHz Radio)
- Protocol bridge (with I2C/SPI bus)
- Smart card

However, application specific and vendor specific are more popular in custom USB devices.