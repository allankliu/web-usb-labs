// Check if Web USB API is supported
if ('usb' in navigator) {
    console.log('Web USB API 是可用的。');
   } else {
    console.log('Web USB API 不可用。');
   }
    
   // 枚举 USB 设备
   function enumerateUSB() {
    navigator.usb.requestDevice({ filters: [{ vendorId: 0x303a }] })
      .then(function(device) {
        console.log('找到设备:', device);
        return device.open();
      })
      .then(function(device) {
        console.log('设备已打开:', device);
        // 在这里可以添加更多的 USB 通信代码
      })
      .catch(function(error) {
        console.error('枚举 USB 设备失败:', error);
      });
   }
    
   // 绑定按钮事件
   document.getElementById('enumerateUSB').addEventListener('click', enumerateUSB);